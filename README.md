**Bem vindo!**

* Para clonar o repositório e instalar as dependencias basta executar: 

```
#!python

curl https://bitbucket.org/robatistello/srv-housepi/raw/36e1f2d50296c4c9d690281482b8d56e7b111731/setup.sh | sudo sh
 
```

* O Apk encontra-se na Google Play: [Baixe aqui](https://play.google.com/store/apps/details?id=br.com.housepi)

**Imagens:**

![Screenshot_2014-05-17-14-07-11.png](https://bitbucket.org/repo/KbG8KA/images/2324044091-Screenshot_2014-05-17-14-07-11.png)![20140509_003926_Android.jpg](https://bitbucket.org/repo/KbG8KA/images/2019319887-20140509_003926_Android.jpg)


**Se for utilizar, referencie o autor!**