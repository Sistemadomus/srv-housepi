#!/bin/bash

echo "Instalando atualizacoes..."
sudo apt-get -y update

sudo rm -r HousePi

sudo apt-get -y install git

echo "Clonando repositorio..."
sudo git clone https://Sistemadomus@bitbucket.org/Sistemadomus/srv-housepi.git

cd /home/pi/srv-housepi

sudo git pull

echo "Criando diretorios..."
sudo mkdir imagens
sudo mkdir musicas
sudo mkdir videos

echo "Configurando permissoes..."
sudo chmod 777 /home/pi/srv-housepi
sudo chmod 777 imagens
sudo chmod 777 musicas	
sudo chmod 777 videos
sudo chmod 777 bin
sudo chmod 777 bd
sudo chmod 777 iniciar.sh
sudo chmod +x iniciar.sh
cd bin 
sudo chmod 777 mjpg-streamer
sudo chmod 777 Adafruit_Python_DHT
sudo chmod 777 pyomxplayer
cd /home/pi/srv-housepi/bin/mjpg-streamer/
sudo chmod 777 mjpg_streamer
sudo chmod 777 mjpg-streamer.sh
sudo chmod +x mjpg-streamer.sh
cd /home/pi/srv-housepi

echo "Instalando bibliotecas GPIO..."
sudo apt-get -y install python-dev
sudo apt-get -y install python-rpi.gpio
sudo sed -i '/i2c-bcm2708/d' /etc/modules
sudo sed -i '/i2c-dev/d' /etc/modules
sudo echo 'i2c-bcm2708' >> /etc/modules
sudo echo 'i2c-dev' >> /etc/modules
sudo apt-get -y install python-smbus
sudo apt-get -y install i2c-tools
sudo sed -i 's/blacklist spi-bcm2708/#blacklist spi-bcm2708/g' /etc/modprobe.d/raspi-blacklist.conf
sudo sed -i 's/blacklist i2c-bcm2708/#blacklist i2c-bcm2708/g' /etc/modprobe.d/raspi-blacklist.conf

sudo sed -i '/dtparam=i2c_arm=on/d' /boot/config.txt
sudo echo 'dtparam=i2c_arm=on' >> /boot/config.txt

echo "Instalando sqlite3..."
sudo apt-get -y install sqlite3

echo "Instalando FTP..." 
sudo apt-get -y install vsftpd 
sudo cp /home/pi/srv-housepi/bin/vsftpd.conf /etc/
sudo /etc/init.d/vsftpd restart

echo "Instalando mjpg-streamer..."
sudo apt-get -y install libjpeg8
sudo apt-get -y install libv41-0

echo "Instalando MPlayer..."
sudo apt-get -y install mplayer mpg123

echo "Instalando OmxPlayer..."
sudo apt-get -y install omxplayer

echo "Instalando fswebcam..."
sudo apt-get -y install fswebcam

echo "Instalando pexpect-2.3..."
cd /home/pi/srv-housepi/bin/pyomxplayer/pexpect-2.3
sudo python ./setup.py install
cd /home/pi/srv-housepi

echo "Instalando DHT driver..."
sudo apt-get install build-essential python-dev
cd /home/pi/srv-housepi/bin/Adafruit_Python_DHT
sudo python ./setup.py install
cd /home/pi/srv-housepi

echo "Configurando tempo do monitor..."
sed -i 's/BLANK_TIME=30/BLANK_TIME=0/g' /etc/kbd/config
sed -i 's/POWERDOWN_TIME=15/POWERDOWN_TIME=0/g' /etc/kbd/config
sed -i 's/POWERDOWN_TIME=30/POWERDOWN_TIME=0/g' /etc/kbd/config

echo "Configurando inicio automatico..."
crontab -l > mycron
echo "@reboot /home/pi/srv-housepi/iniciar.sh" >> mycron
crontab mycron
rm mycron

echo "Instalacao concluida."

sudo reboot
