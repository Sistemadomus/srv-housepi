#!/usr/bin/python
#-*- coding: utf-8 -*-

import thread
import threading
import sys
import Funcoes
import xml.etree.ElementTree as ET
import Adafruit_DHT
import time
import json
import os

from datetime import date, datetime
from xml.etree.ElementTree import Element

ARQUIVO_JSON = os.path.dirname(os.path.abspath(__file__)) + "/historico.json"

class Historico:
    def __init__(self, hora, temperatura, humidade):
        self.hora        = hora        
        self.temperatura = temperatura
        self.humidade    = humidade
        
class TemperaturaHumidade(threading.Thread):
    
    def __init__(self):
        threading.Thread.__init__(self)
        
        self.name                = 'ThreadTemperaturaHumidade'
        self.__stop_thread_event = threading.Event()
        self.GPIODHT             = int(Funcoes.lerConfiguracaoIni("GPIODHT"))
        self.aguardar            = False
        self.historico           = []

        self.carregarDados()

    def stop(self):
        self.__stop_thread_event.set()
        
    def run(self):
        self.__lerSensorThread()

    def salvarDados(self):
        lista = []
        
        for item in self.historico:
            lista.append(item.__dict__)
        
        with open(ARQUIVO_JSON, "w") as dataFile:
            json.dump(lista, dataFile, indent=4)

    def carregarDados(self):
        if os.path.isfile(ARQUIVO_JSON):
            with open(ARQUIVO_JSON) as dataFile:    
                self.historico = json.load(dataFile, object_hook=jsonHistorico)
    
    def __lerSensor(self):
        try:
            sensor                = Adafruit_DHT.DHT22
            humidade, temperatura = Adafruit_DHT.read_retry(sensor, self.GPIODHT) 
            
            return (humidade, temperatura)
        except:
            return (None, None)

    def __lerSensorThread(self):
        tempoDecorido = 0

        while not self.__stop_thread_event.isSet():
            if not self.aguardar and ((len(self.historico) == 0) or (tempoDecorido / 60 >= 60)):
                humidade, temperatura = self.__lerSensor()

                if humidade is not None and temperatura is not None and not self.aguardar:
                    tempoDecorido = 0
                    hora          = datetime.now().strftime("%H:%M")
                    historico     = Historico(hora, temperatura, humidade)

                    self.historico.insert(len(self.historico), historico)

                    if len(self.historico) > 5:
                        del self.historico[0]

                    self.salvarDados()
            
            time.sleep(1)
            
            tempoDecorido = tempoDecorido + 1
            
    def getDados(self):
        try:
            self.aguardar = True

            try:
                humidade, temperatura = self.__lerSensor()

                if humidade is not None and temperatura is not None:
                    hora  = datetime.now().strftime("%H:%M")
                    root  = Element("TemperaturaHumidade")
                    dados = Element("Dados", Temperatura="%.1f" % temperatura, Humidade="%.1f" % humidade)
                        
                    root.append(dados)

                    historico = Element("Historico")

                    for registro in self.historico:
                        historico.append(Element("Historico", 
                            Hora=str(registro.hora), 
                            Temperatura=str(registro.temperatura), 
                            Humidade=str(registro.humidade)))
                        
                    historico.append(Element("Historico", 
                        Hora=str(hora), 
                        Temperatura=str(temperatura), 
                        Humidade=str(humidade)))

                    root.append(historico)

                    xmlstr = ET.tostring(root) + "\n"

                    return xmlstr
                else:
                    return "Erro\n"
            except Exception, e:
                print e 
        finally:
            self.aguardar = False

def jsonHistorico(item):
    novoHistorico = Historico(item['hora'], item['temperatura'], item['humidade'])
    
    return novoHistorico
