#!/usr/bin/python
#-*- coding: utf-8 -*-

import Base

class Usuario(Base.Base):
    
    def __init__(self):
        self.carregarUsuario()
            
    def carregarUsuario(self):
        row = self.consultarRegistro("select Usuario, Senha from Usuario")
        
        self.usuario = row["Usuario"]
        self.senha   = row["Senha"]        
    
    def alterarUsuarioSenha(self, usuario, senha):        
        sql = "update Usuario set Usuario = '{novoUsuario}', Senha = '{novaSenha}'"
        sql = sql.format(novoUsuario =  usuario, novaSenha = senha)
        
        if self.executarComando(sql):
            self.usuario = usuario
            self.senha   = senha
            
            return True
        else:
            return False        
    
    def validarLogin(self, usuario, senha):
        if (self.usuario == usuario) and (self.senha == senha):
            return True
        else:
            return False