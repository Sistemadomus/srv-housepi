#!/usr/bin/python
#-*- coding: utf-8 -*-

import Funcoes
import os
import Base

class Camera(Base.Base):
    
    def __init__(self, usuario):
        self.MJPG    = Funcoes.lerConfiguracaoIni("CaminhoMJPG")
        self.usuario = usuario
        
    def ligar(self):
        rows = self.consultarRegistros("select * from Camera")

        for row in rows:
            autenticacao    = self.usuario.usuario + ":" + self.usuario.senha
            resolucaoFrames = str(row["Resolucao"]) + " " + str(row["Frames"])

            if int(row["YUV"]) == 1:
                yuv = "true"
            else:
                yuv = "false"
 
            os.system("sudo " + self.MJPG + " start " +  str(row["Porta"]) + " " + resolucaoFrames + " " + str(row["Device"]) + " " + autenticacao + " " +  yuv) 
        
    def desligar(self):
        rows = self.consultarRegistros("select * from Camera") 
        
        for row in rows:    
            os.system("sudo " + self.MJPG + " stop " + str(row["Device"]))
       
    def CapturarImagem(self, device, nome):
        os.system("sudo fswebcam -r 320x240 -d " + str(device) + " --no-banner " + str(nome))