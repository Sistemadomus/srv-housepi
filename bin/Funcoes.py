#!/usr/bin/python
#-*- coding: utf-8 -*-

import ConfigParser
import os
import logging

from unicodedata import normalize

DIRETORIO = os.path.dirname(os.path.abspath(__file__))
CONFIG    = DIRETORIO + '/Config.ini'
NOME_LOG  = DIRETORIO + '/housepi.log'

def removerAcentos(txt, codif='utf-8'):
    if  isinstance(txt, unicode):
        return normalize('NFKD', txt).encode('ASCII','ignore')
    else:
        return normalize('NFKD', txt.decode(codif)).encode('ASCII','ignore')

def stringToList(texto):
    lista = []
    lista = texto.split(';')

    return lista

def lerConfiguracaoIni(nome):
    cfg = ConfigParser.ConfigParser()
    cfg.read(CONFIG)

    return cfg.get('Dados', nome)

def alterarConfiguracaoIni(nome, valor):  
    cfg = ConfigParser.ConfigParser()
    cfg.read(CONFIG)
    cfg.set('Dados', nome, valor)
    
    file = open(CONFIG, 'wb')
    
    cfg.write(file)

def getSerial():     
    cpuserial = "0000000000000000"
  
    try:
        arquivo = open('/proc/cpuinfo','r')
    
        for line in arquivo:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26]
    
        arquivo.close()
    except:
        cpuserial = "ERROR000000000"

    return cpuserial  

def verificarLog():
	if os.path.isfile(NOME_LOG):
		os.remove(NOME_LOG)
	
def salvarLog(texto):
    logging.basicConfig(format='%(asctime)s %(message)s ', datefmt='%d/%m/%Y %H:%M:%S', filename=NOME_LOG, level=logging.DEBUG)
    logging.info(removerAcentos(texto))

def pegarLog():
    if os.path.isfile(NOME_LOG):
        lista = []
 
        arquivo = open(NOME_LOG, 'r')
        
        for linha in arquivo.xreadlines():
            lista.insert(len(lista) + 1, linha.rstrip())

        arquivo.close()

        return lista
