#!/usr/bin/python
#-*- coding: utf-8 -*-

import sqlite3
import Funcoes
import os
import os.path

NOME_BANCO   = os.path.dirname(os.path.abspath(__file__)) + "/HousePi.db"
SCRIPT_BANCO = os.path.dirname(os.path.abspath(__file__)) + "/ScriptBanco.sql"

class Base(object):

    def verificarBanco(self):
        if not (os.path.exists(NOME_BANCO)):
            try:
                sql = open(SCRIPT_BANCO, "r").read()

                conBanco = self.conectarBanco()
                cursor   = conBanco.cursor()

                cursor.executescript(sql)
            except Exception, e:
                print "Erro criando o banco de dados. ", e
            finally:
                cursor.close()        
    
    def conectarBanco(self):
        try:
            conBanco              = sqlite3.connect(NOME_BANCO)
            conBanco.row_factory  = sqlite3.Row
            conBanco.text_factory = str
        except Exception, e:
            print "Nao foi possivel conectar ao banco de dados. ", e
    
        return conBanco
    
    def executarComando(self, sql):
        try:
            conBanco = self.conectarBanco()
            cursor   = conBanco.cursor()
            
            cursor.execute(sql)
            conBanco.commit()
            conBanco.close()
            
            return True
        except:
            conBanco.rollback()
            conBanco.close()
            
            print "Erro ao executar o comando: " + sql
            return False
    
    def consultarRegistro(self, sql):
        try:
            conBanco = self.conectarBanco()
            cursor   = conBanco.cursor()
            
            cursor.execute(sql)
            row = cursor.fetchone()
            conBanco.close()
        
            return row
        except:
            print "Erro ao executar o comando: " + sql
            return None
    
    def consultarRegistros(self, sql):
        try:
            conBanco = self.conectarBanco()
            cursor   = conBanco.cursor()
            
            cursor.execute(sql)
            rows = cursor.fetchall()
            conBanco.close()
            
            return rows
        except:
            print "Erro ao executar o comando: " + sql
            return None